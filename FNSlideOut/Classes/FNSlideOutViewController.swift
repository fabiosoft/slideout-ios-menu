//
//  FNSlideOutViewController.swift
//  FNSlideOut
//
//  Created by Fabio Nisci on 08/10/18.
//

import UIKit

typealias FNSideView = UIView
typealias FNMasterView = UIView

extension FNSlideOutViewController {
    public static var didOpenFNSlideOutNotificationName: NSNotification.Name{
        return NSNotification.Name(rawValue: "didOpenFNSlideOutNotificationName")
    }
    public static var willOpenFNSlideOutNotificationName: NSNotification.Name{
        return NSNotification.Name(rawValue: "willOpenFNSlideOutNotificationName")
    }
    public static var didCloseFNSlideOutNotificationName: NSNotification.Name{
        return NSNotification.Name(rawValue: "didOpenFNSlideOutNotificationName")
    }
    public static var willCloseFNSlideOutNotificationName: NSNotification.Name{
        return NSNotification.Name(rawValue: "willOpenFNSlideOutNotificationName")
    }
}

struct FNSlideOutConstants {
    static let sideViewWidth: CGFloat = min((UIApplication.shared.keyWindow?.frame.size.width ?? 300) * 0.8, 300)
    static let velocityThreshold: CGFloat = 500
    
}

public protocol FNSlideOutViewControllerDelegate:class {
    func didTapToClose(_ controller:FNSlideOutViewController)
}

@objc public class FNSlideOutViewController: UIViewController {
    
    private var masterViewLeadingConstraint: NSLayoutConstraint!
    private var masterViewTrailingConstraint: NSLayoutConstraint!
    private var isMenuOpened = false
    
    private var sideMenuViewController:UIViewController!
    private var masterViewController:UIViewController!
    public weak var delegate:FNSlideOutViewControllerDelegate?
    
    public init(sideMenuViewController: UIViewController, masterViewController: UIViewController, delegate:FNSlideOutViewControllerDelegate? = nil) {
        super.init(nibName: nil, bundle: nil)
        self.sideMenuViewController = sideMenuViewController
        self.masterViewController = masterViewController
        self.delegate = delegate
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let masterView:FNMasterView = {
        let view = FNMasterView()
        view.backgroundColor = .red
        view.translatesAutoresizingMaskIntoConstraints = false
        view.accessibilityIdentifier = "fnslideout.masterView"
        return view
    }()
    
    let sideView:FNSideView = {
        let view = FNSideView()
        view.backgroundColor = .blue
        view.translatesAutoresizingMaskIntoConstraints = false
        view.accessibilityIdentifier = "fnslideout.sideview"
        return view
    }()
    
    private let darkCoverView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.7)
        view.alpha = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        view.backgroundColor = .white
    }
    
    private func setupViews(){
        view.addSubview(masterView)
        view.addSubview(sideView)
        
        NSLayoutConstraint.activate([
            masterView.topAnchor.constraint(equalTo: view.topAnchor),
            masterView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            sideView.topAnchor.constraint(equalTo: view.topAnchor),
            sideView.trailingAnchor.constraint(equalTo: masterView.leadingAnchor),
            sideView.widthAnchor.constraint(equalToConstant: FNSlideOutConstants.sideViewWidth),
            sideView.bottomAnchor.constraint(equalTo: masterView.bottomAnchor)
            ])
        
        masterViewLeadingConstraint = masterView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0)
        masterViewLeadingConstraint.isActive = true
        
        masterViewTrailingConstraint = masterView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        masterViewTrailingConstraint.isActive = true
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        view.addGestureRecognizer(panGesture)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapDismiss(_:)))
        darkCoverView.addGestureRecognizer(tapGesture)
        
        setupViewControllers()
    }
    
    private func setupViewControllers(){
        let masterControllerView = self.masterViewController.view!
        let sideMenuControllerView = self.sideMenuViewController.view!
        
        masterControllerView.translatesAutoresizingMaskIntoConstraints = false
        sideMenuControllerView.translatesAutoresizingMaskIntoConstraints = false
        
        masterView.addSubview(masterControllerView)
        masterView.addSubview(darkCoverView)
        sideView.addSubview(sideMenuControllerView)
        
        NSLayoutConstraint.activate([
            // top, leading, bottom, trailing anchors
            masterControllerView.topAnchor.constraint(equalTo: masterView.topAnchor),
            masterControllerView.leadingAnchor.constraint(equalTo: masterView.leadingAnchor),
            masterControllerView.bottomAnchor.constraint(equalTo: masterView.bottomAnchor),
            masterControllerView.trailingAnchor.constraint(equalTo: masterView.trailingAnchor),
            
            sideMenuControllerView.topAnchor.constraint(equalTo: sideView.topAnchor),
            sideMenuControllerView.leadingAnchor.constraint(equalTo: sideView.leadingAnchor),
            sideMenuControllerView.bottomAnchor.constraint(equalTo: sideView.bottomAnchor),
            sideMenuControllerView.trailingAnchor.constraint(equalTo: sideView.trailingAnchor),
            
            darkCoverView.topAnchor.constraint(equalTo: masterView.topAnchor),
            darkCoverView.leadingAnchor.constraint(equalTo: masterView.leadingAnchor),
            darkCoverView.bottomAnchor.constraint(equalTo: masterView.bottomAnchor),
            darkCoverView.trailingAnchor.constraint(equalTo: masterView.trailingAnchor),
            ])
        
        addChild(self.masterViewController)
        addChild(self.sideMenuViewController)
    }
    
    public func openMenu() {
        masterViewLeadingConstraint.constant = FNSlideOutConstants.sideViewWidth
        masterViewTrailingConstraint.constant = FNSlideOutConstants.sideViewWidth
        isMenuOpened = true
        NotificationCenter.default.post(name: FNSlideOutViewController.willOpenFNSlideOutNotificationName, object: nil)
        performAnimations()
    }
    
    public func closeMenu() {
        masterViewLeadingConstraint.constant = 0
        masterViewTrailingConstraint.constant = 0
        isMenuOpened = false
        NotificationCenter.default.post(name: FNSlideOutViewController.willCloseFNSlideOutNotificationName, object: nil)
        performAnimations()
    }
    
    private func performAnimations() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
            self.darkCoverView.alpha = self.isMenuOpened ? 1 : 0
        }, completion: { (finished) in
            if(self.isMenuOpened){
                NotificationCenter.default.post(name: FNSlideOutViewController.didOpenFNSlideOutNotificationName, object: nil)
            }else{
                NotificationCenter.default.post(name: FNSlideOutViewController.didCloseFNSlideOutNotificationName, object: nil)
            }
        })
    }
    
    //MARK: - panning
    @objc private func handlePan(_ gesture:UIPanGestureRecognizer){
        let translation = gesture.translation(in: view)
        let velocity = gesture.velocity(in: view)
        
        switch gesture.state {
        case .changed:
            var x = translation.x
            
            x = isMenuOpened ? x + FNSlideOutConstants.sideViewWidth : x
            x = min(FNSlideOutConstants.sideViewWidth, x)
            x = max(0, x)
            let progress = x / FNSlideOutConstants.sideViewWidth
            
            masterViewLeadingConstraint.constant = x
            masterViewTrailingConstraint.constant = x
            darkCoverView.alpha = progress
            break
            
        case .ended:
            if isMenuOpened {
                if velocity.x < -FNSlideOutConstants.velocityThreshold {
                    closeMenu()
                    return
                }
                if abs(translation.x) < FNSlideOutConstants.sideViewWidth / 2 {
                    openMenu()
                } else {
                    closeMenu()
                }
            } else {
                if velocity.x > FNSlideOutConstants.velocityThreshold {
                    openMenu()
                    return
                }
                
                if translation.x < FNSlideOutConstants.sideViewWidth / 2 {
                    closeMenu()
                } else {
                    openMenu()
                }
            }
            break
        default:
            break
        }
    }
    
    @objc private func handleTapDismiss(_ gesture:UITapGestureRecognizer){
        self.delegate?.didTapToClose(self)
    }
}
