# FNSlideOut

[![CI Status](https://img.shields.io/travis/Fabio Nisci/FNSlideOut.svg?style=flat)](https://travis-ci.org/Fabio Nisci/FNSlideOut)
[![Version](https://img.shields.io/cocoapods/v/FNSlideOut.svg?style=flat)](https://cocoapods.org/pods/FNSlideOut)
[![License](https://img.shields.io/cocoapods/l/FNSlideOut.svg?style=flat)](https://cocoapods.org/pods/FNSlideOut)
[![Platform](https://img.shields.io/cocoapods/p/FNSlideOut.svg?style=flat)](https://cocoapods.org/pods/FNSlideOut)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FNSlideOut is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FNSlideOut'
```

## Author

Fabio Nisci, fabionisci@gmail.com

## License

FNSlideOut is available under the MIT license. See the LICENSE file for more info.
