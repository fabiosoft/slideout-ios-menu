//
//  FNSlideOut_UITests.swift
//  FNSlideOut_UITests
//
//  Created by Fabio Nisci on 14/10/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import XCTest

class FNSlideOut_UITests: XCTestCase {
    var app: XCUIApplication!
    private var rootWindow: UIWindow!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        rootWindow = UIWindow(frame: UIScreen.main.bounds)

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app.launch()
        

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        XCUIDevice.shared.orientation = .portrait
        rootWindow = nil
    }

    func test_masterView_and_sideView_open_close() {
        XCUIDevice.shared.orientation = .portrait
        
        let masterView = app.otherElements["fnslideout.masterView"]
        let sideView = app.otherElements["fnslideout.sideview"]
        XCTAssert(sideView.waitForExistence(timeout: 5))
        XCTAssert(masterView.waitForExistence(timeout: 5))
//        XCTAssertEqual(rootWindow.frame, masterView.frame)
        
        open_close_interaction(sideView: sideView, masterView: masterView)
        
        XCUIDevice.shared.orientation = .landscapeRight
        
        open_close_interaction(sideView: sideView, masterView: masterView)
        
    }
    
    private func open_close_interaction(sideView: XCUIElement, masterView: XCUIElement){
        
        XCTAssertLessThan(sideView.frame.origin.x, masterView.frame.origin.x, "should be out of screen")
        masterView.swipeRight()
        XCTAssertEqual(sideView.frame.origin.x, 0, "should be on screen")
        XCTAssertLessThanOrEqual(sideView.frame.size.width, 300, "too large")
        masterView.tap()
        XCTAssertLessThan(sideView.frame.origin.x, masterView.frame.origin.x, "should be out of screen")
    }

}
